// -------------------------------------------------------------------------------------------------
// SPDX-License-Identifier: Apache-2.0
// Copyright (C) 2024 Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -------------------------------------------------------------------------------------------------

#ifndef MATH_HPP
#define MATH_HPP

namespace A {

auto square(int x) -> int;
auto cube(int x) -> int;
auto quadratic(int x) -> int;
} // namespace A

#endif // MATH_HPP
