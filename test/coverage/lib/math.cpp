// -------------------------------------------------------------------------------------------------
// SPDX-License-Identifier: Apache-2.0
// Copyright (C) 2024 Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -------------------------------------------------------------------------------------------------

#include <math.hpp>

namespace A {
auto square(int x) -> int
{
  return x * x;
}

auto cube(int x) -> int
{
  return x * x * x;
}

auto quadratic(int x) -> int
{
  return x * x * x * x;
}
} // namespace A
