// -------------------------------------------------------------------------------------------------
// SPDX-License-Identifier: Apache-2.0
// Copyright (C) 2024 Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -------------------------------------------------------------------------------------------------

auto main(int argc, char** /*argv*/) -> int
{
  int* array = new int[100]; // NOLINT
  delete[] array;            // NOLINT
  return array[argc];        // NOLINT
}
