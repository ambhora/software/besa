// -------------------------------------------------------------------------------------------------
// SPDX-License-Identifier: Apache-2.0
// Copyright (C) 2024 Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -------------------------------------------------------------------------------------------------

// NOLINTNEXTLINE
void* p;

// NOLINTNEXTLINE
int main()
{
  return 0;
}
