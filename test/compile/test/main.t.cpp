// -------------------------------------------------------------------------------------------------
// SPDX-License-Identifier: Apache-2.0
// Copyright (C) 2024 Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -------------------------------------------------------------------------------------------------

#include <iostream>

int satellite_function();

int main()
{
  std::cout << "Hello, World!" << std::endl;
  if (satellite_function() != 42) {
    std::cerr << "Satellite function failed!" << std::endl;
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}
