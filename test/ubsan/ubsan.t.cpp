// -------------------------------------------------------------------------------------------------
// SPDX-License-Identifier: Apache-2.0
// Copyright (C) 2024 Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -------------------------------------------------------------------------------------------------

// NOLINTNEXTLINE
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{
  // NOLINTNEXTLINE
  [[maybe_unused]] int k = 1;
  // NOLINTNEXTLINE
  k += argc;
  return 0;
}
