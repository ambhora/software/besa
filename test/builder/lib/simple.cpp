// -------------------------------------------------------------------------------------------------
// SPDX-License-Identifier: Apache-2.0
// Copyright (C) 2024 Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -------------------------------------------------------------------------------------------------

#include <simple.hpp>

double f(int i, double g)
{
  double value = 1;
  while (i > 0) {
    value *= g;
    --i;
  }
  return value;
}
