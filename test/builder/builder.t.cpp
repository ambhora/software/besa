// -------------------------------------------------------------------------------------------------
// SPDX-License-Identifier: Apache-2.0
// Copyright (C) 2024 Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -------------------------------------------------------------------------------------------------

#include <cstdlib>
#include <simple.hpp>

auto main() -> int
{
  if (f(2, 2.0) != 4.0) {
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
