// -------------------------------------------------------------------------------------------------
// SPDX-License-Identifier: Apache-2.0
// Copyright (C) 2024 Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -------------------------------------------------------------------------------------------------

#ifndef SIMPLE_HPP
#define SIMPLE_HPP

double f(int i, double g);

#endif // SIMPLE_HPP
