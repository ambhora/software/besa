# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2024 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------
include_guard(GLOBAL)

function(besa_coverage_register_test GROUP_NAME TEST_NAME TARGET_NAME)
endfunction()

function(besa_coverage_register_test_core GROUP_NAME TEST_NAME TARGET_NAME)
endfunction()

function(besa_coverage_initialize_group GROUP_NAME TEST_NAME TARGET_NAME)
endfunction()
